//
//  DetailViewController.swift
//  MarvelApp
//
//  Created by Sebastian Guerrero on 1/3/18.
//  Copyright © 2018 SG. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
  
  var pokemon:Pokemon?
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    print(pokemon?.name)
    self.title = pokemon?.name
  }
  
}
