//
//  Pokemon.swift
//  MarvelApp
//
//  Created by Sebastian Guerrero on 11/29/17.
//  Copyright © 2017 SG. All rights reserved.
//

import Foundation
import ObjectMapper

class Pokemon: Mappable {
  
  var name:String?
  var height:Double?
  var weight:Double?
  var id:Int?
  
  required init?(map: Map) {
  }
  
  func mapping(map: Map) {
    id <- map["id"]
    name <- map["name"]
    height <- map["height"]
    weight <- map["weight"]
  }
  
  
}
