//
//  ViewController.swift
//  MarvelApp
//
//  Created by Sebastian Guerrero on 11/28/17.
//  Copyright © 2017 SG. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {
  
  @IBOutlet weak var nameLabel: UILabel!
  
  @IBOutlet weak var heightLabel: UILabel!
  
  @IBOutlet weak var weightLabel: UILabel!
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }
  
  @IBAction func consultarButtonPressed(_ sender: Any) {
    
    let pkId = arc4random_uniform(250) + 1
    Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(pkId)").responseObject { (response: DataResponse<Pokemon>) in
      
      let pokemon = response.result.value
      
      DispatchQueue.main.async {
        self.nameLabel.text = pokemon?.name ?? ""
        self.heightLabel.text = "\(pokemon?.height ?? 0)"
        self.weightLabel.text = "\(pokemon?.weight ?? 0)"
      }
    }
  }
}

