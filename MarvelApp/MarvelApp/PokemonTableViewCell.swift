//
//  PokemonTableViewCell.swift
//  MarvelApp
//
//  Created by Sebastian Guerrero on 1/3/18.
//  Copyright © 2018 SG. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {
  
  
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var heightLabel: UILabel!
  @IBOutlet weak var weightLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
  }
  
  func fillData(pokemon:Pokemon){
    nameLabel.text = pokemon.name
    heightLabel.text = "\(pokemon.height ?? 0)"
    weightLabel.text = "\(pokemon.weight ?? 0)"
  }
  
}
