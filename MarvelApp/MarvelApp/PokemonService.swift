//
//  PokemonService.swift
//  MarvelApp
//
//  Created by Sebastian Guerrero on 1/2/18.
//  Copyright © 2018 SG. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

protocol PokemonServiceDelegate {
  func get20FirstPokemons(pokemons:[Pokemon])
}

class PokemonService {
  
  var delegate:PokemonServiceDelegate?
  
  func downloadPokemons(){
    
    var pokemonArray:[Pokemon] = []
    let dpGR = DispatchGroup()
    for i in 1...20 {
      dpGR.enter()
      Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseObject { (response: DataResponse<Pokemon>) in
        
        let pokemon = response.result.value
        pokemonArray.append(pokemon!)
        dpGR.leave()
      }
    }
    
    dpGR.notify(queue: .main){
      let sortedArray = pokemonArray.sorted(by: {$0.id! < $1.id!})
      self.delegate?.get20FirstPokemons(pokemons: sortedArray)
    }
    
  }
}
